FROM alpine:$$ALPINE_VERSION
LABEL maintainer "Raphaël Droz <raphael.droz@gmail.com>"

ADD https://packages.whatwedo.ch/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub
RUN echo https://dl-cdn.alpinelinux.org/alpine/edge/testing >> /etc/apk/repositories
RUN apk --no-cache add ca-certificates less bash wget curl git make sed nano jq zip subversion yarn socat fping patch \
    mariadb-client perl-xml-xpath perl-xml-libxslt libxslt perl-file-rename \
    php$$PHP_VERSION \
    php$$PHP_VERSION-ctype \
    php$$PHP_VERSION-curl \
    php$$PHP_VERSION-dom \
    php$$PHP_VERSION-fileinfo \
    php$$PHP_VERSION-fpm \
    php$$PHP_VERSION-gd \
    php$$PHP_VERSION-iconv \
    php$$PHP_VERSION-json \
    php$$PHP_VERSION-mbstring \
    php$$PHP_VERSION-mysqli \
    php$$PHP_VERSION-openssl \
    php$$PHP_VERSION-pdo_mysql \
    php$$PHP_VERSION-pdo_sqlite \
    php$$PHP_VERSION-pecl-imagick \
    php$$PHP_VERSION-pecl-pcov \
    php-pecl-redis \
    php$$PHP_VERSION-pecl-xdebug \
    php$$PHP_VERSION-phar \
    php$$PHP_VERSION-session \
    php$$PHP_VERSION-simplexml \
    php$$PHP_VERSION-soap \
    php$$PHP_VERSION-tokenizer \
    php$$PHP_VERSION-xml \
    php$$PHP_VERSION-xmlreader \
    php$$PHP_VERSION-xmlwriter \
    php$$PHP_VERSION-xsl \
    php$$PHP_VERSION-zip

# wp-cli/WordPress testsuite dep'. xsltproc/xpath is useful for WP XHR dumps
# mysql-client xsltproc libxml-xpath-perl python3-rjsmin

# Useful phar: Behat, phpunit, phpcs, ...
RUN echo "date.timezone = Europe/Paris" | tee /etc/php$$PHP_VERSION/conf.d/80_timezone.ini \
    && curl -sSLo /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-$$PHPUNIT_VERSION.phar \
    && curl -sSLo /usr/local/bin/composer1 https://getcomposer.org/download/1.10.26/composer.phar \
    && curl -sSLo /usr/local/bin/composer https://getcomposer.org/download/2.7.1/composer.phar \
    && curl -sSLo /usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar \
    && curl -sSLo /usr/local/bin/phpcbf https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar \
    && curl -sSLo /usr/local/bin/wp https://github.com/wp-cli/wp-cli/releases/download/v2.10.0/wp-cli-2.10.0.phar \
    && curl -sSLo /usr/local/bin/behat https://github.com/Behat/Behat/releases/download/v3.13.0/behat.phar \
    && chmod 755 /usr/local/bin/*

RUN curl -sLo /tmp/symfony-cli.apk https://github.com/symfony-cli/symfony-cli/releases/download/v5.8.14/symfony-cli_5.8.14_x86_64.apk && apk add --allow-untrusted /tmp/symfony-cli.apk

RUN ln -s /usr/bin/php$$PHP_VERSION /usr/bin/php || true
